# zerohacks.php5-fpm

[![Build Status](https://travis-ci.org/zerohacks/ansible-php5-fpm.svg?branch=master)](https://travis-ci.org/zerohacks/ansible-php5-fpm)
[![Ansible Role](https://img.shields.io/ansible/role/10680.svg?maxAge=2592000)](https://galaxy.ansible.com/zerohacks/php5-fpm/)

Install PHP 5.6

- php5.6-cli
- php5.6-fpm
- php5.6-mbstring
- php5.6-intl
- php5.6-curl
- php5.6-gd
- php5.6-mcrypt
- php5.6-mysql
- php5.6-sqlite3
- php5.6-xml
- php5.6-zips
- php-memcached
- php-xdebug

## Requirements

This role requires Ansible 1.9 or higher and platform requirements are listed in the metadata file.

## Role Variables

- `timezone` (default: "Europe/Moscow")
- `fpm_listen` (default: "127.0.0.1:9000")

## Dependencies

None

## Example Playbook

    - hosts: all
      roles:
        - role: zerohacks.php5-fpm
          vars:
            - timezone: Europe/Moscow
            - fpm_listen: 127.0.0.1:9000

## License

Licensed under the MIT license: http://www.opensource.org/licenses/mit-license.php
